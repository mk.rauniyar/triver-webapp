import React from 'react';
import Paper from '@material-ui/core/Paper';
import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TextField from '@material-ui/core/TextField';

const styles = theme => ({
    root: {
        width: '100%',
        marginTop: theme.spacing.unit * 3,
        overflowX: 'auto',
    },
    table: {
        minWidth: 700,
    },
    paper: {
        padding: 30,
        textAlign: 'center',
        color: 'red',
    },
    pageTitle: {
        paddingTop: 10,
        paddingBottom: 20
    }
});



let id = 0;
function createData(tourId, date, miles, tour, maps) {
    id += 1;
    return { id, tourId, date, miles, tour, maps };
}

const rows = [
    createData('#343434', '09/14/2018', 6.0, 24, 'link'),
    createData('#566563', '09/15/2018', 9.0, 37, 'link'),
    createData('#887393', '09/18/2018', 16.0, 24, 'link'),
    createData('#233233', '09/20/2018', 3.7, 67, 'link'),
    createData('#343434', '09/30/2018', 16.0, 49, 'link'),
];

const tourHistory = (props) => {
    const { classes } = props;
    return (
        <div className={classes.background}>
            <div className="container">
                <h2 className={classes.pageTitle}>My Rides</h2>
                <Paper className={classes.root}>
                    <Table className={classes.table}>
                        <TableHead>
                            <TableRow>
                                <TableCell>Tour ID</TableCell>
                                <TableCell numeric>Date</TableCell>
                                <TableCell numeric>Miles</TableCell>
                                <TableCell numeric>Hour</TableCell>
                                <TableCell numeric>Map Links</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {rows.map(row => {
                                return (
                                    <TableRow key={row.id}>
                                        <TableCell component="th" scope="row">
                                            {row.tourId}
                                        </TableCell>
                                        <TableCell numeric>{row.date}</TableCell>
                                        <TableCell numeric>{row.miles}</TableCell>
                                        <TableCell numeric>{row.tour}</TableCell>
                                        <TableCell numeric>{row.maps}</TableCell>
                                    </TableRow>
                                );
                            })}
                        </TableBody>
                    </Table>
                </Paper>

            </div>
        </div>
    )


};

// export default tourHistory
export default withStyles(styles)(tourHistory);



