import React from 'react';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import IconButton from '@material-ui/core/IconButton';
import InputAdornment from '@material-ui/core/InputAdornment';
import TextField from '@material-ui/core/TextField';
import MenuItem from '@material-ui/core/MenuItem';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import './../../App.css';
import { withStyles } from '@material-ui/core/styles';


const styles = theme => ({
    root: {
        flexGrow: 1,
    },
    paper: {
        padding: 30,
        textAlign: 'center',
        color: 'red',
    },
    pageTitle: {
        paddingTop: 10,
        paddingBottom: 20
    }
});
const referral = (props) => {

    const { classes } = props;
    const handleClickSubmit = () => {
        console.log('hi');
    }

    return (

        <div className={classes.background}>
            <div className="container">
                <h2 className={classes.pageTitle}>Referrals</h2>
                <Paper className={classes.root}>
                    <Grid container spacing={24} style={{ paddingLeft: 30 }}>
                        <Grid item xs={12} sm={6}>
                            <h5>
                                <a>Refer your friend and get free first Trip</a>
                            </h5>

                            <form className={classes.container} noValidate autoComplete="off">
                                <TextField
                                    id="outlined-name"
                                    label="Friend's Name"
                                    margin="normal"
                                    variant="outlined"
                                    fullWidth
                                />
                                <TextField
                                    id="outlined-name"
                                    label="Friend's Email"
                                    margin="normal"
                                    variant="outlined"
                                    fullWidth

                                />
                                <Button size="big" variant="contained" color="primary" className={classes.button}>
                                    Refer
                                            </Button>
                            </form>

                        </Grid>
                    </Grid>
                </Paper>
            </div>
        </div >

    )
};

// export default referral
export default withStyles(styles)(referral);