import React from 'react';

import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
    root: {
        flexGrow: 1,
    },
    paper: {
        padding: 30,
        textAlign: 'center',
        color: 'red',
    },
    pageTitle: {
        paddingTop: 10,
        paddingBottom: 20
    }
});

const faq = (props) => {
    const { classes } = props;
    return (

        <div className={classes.background}>
            <div className="container">
                <h2 className={classes.pageTitle}>FAQ</h2>
                <Paper className={classes.root}>
                    <Grid container spacing={24} style={{ paddingLeft: 30 }}>
                        <div style={{ margin: 40 }}>
                            <h2>Q1. How to sign up?</h2>
                            <span>
                                </span>
                            <br />
                            <br />
                            <h2>Q2. How are trivers different than UBER and Lyft drivers?</h2>
                            <span> </span>
                            <br />
                            <br />
                            <h2>Q3. How we train Trivers?</h2>
                            <span></span>
                            <br />
                            <br />
                            <h2>Q4. What are the requirements of a Triver?</h2>
                            <span></span>
                            <br />
                            <br />
                            <h2>Q5. What are the benefits of being a Triver?</h2>
                            <span>
                                </span>
                            <br />
                            <br />
                            <h2>Q6. Action against triver?</h2>
                            <span>
                            </span>
                            <br />
                            <br />
                            <h2>Q7. Why to suspend triver?</h2>
                            <span> </span>
                            <br />
                            <br />
                            <h2>Q8. Under what condition can Triver cancel the ongoing trip?</h2>
                            <span> </span>
                            <br />
                            <br />
                            <h2>Q9. What should Triver do when Rider cancel ongoing ride?</h2>
                            <span></span>
                            <br />
                            <br />
                            <h2>Q10. What does Triver do when he/she cancel the rider after arriving at the pick-up location?</h2>
                            <span></span>
                            <br />
                            <br />
                            <h2>Q11. What does Triver do when rider cancels the rider after he/she arrived at the pick-up location?</h2>
                            <span></span>
                            <br />
                            <br />
                            <h2>Q12. How quickly to respond to Trivers concern?</h2>
                            <span></span>
                            <br />
                            <br />
                            <h2>Q13. How should triver be paid?</h2>
                            <span></span>
                            <br />
                            <br />
                            <h2>Q14. Refund policy?</h2>
                            <span></span>
                            <br />
                            <br />
                            <h2>Q15. Unexpected traffic jam.</h2>
                            <span>  </span>
                            <br />
                            <br />
                            <h2>Q16: Car accident by Triver.</h2>
                            <span> </span>
                            <br />
                            <br />
                            <h2>Q17. Car accident from other</h2>
                            <span></span>
                            <br />
                            <br />
                            <h2>Q18. Immediate Contact</h2>
                            <span></span>
                            <br />
                            <br />
                        </div>
                    </Grid>
                </Paper>
            </div>
        </div>
    )


};

// export default faq
export default withStyles(styles)(faq);




