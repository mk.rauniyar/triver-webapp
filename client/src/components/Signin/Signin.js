import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import IconButton from '@material-ui/core/IconButton';
import InputAdornment from '@material-ui/core/InputAdornment';
import TextField from '@material-ui/core/TextField';
import MenuItem from '@material-ui/core/MenuItem';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import './../../App.css';
import axios from 'axios';
import SigninForm from './SigninForm'


const styles = theme => ({
    root: {
        flexGrow: 1,
    },
    paper: {
        textAlign: 'center',
        color: theme.palette.text.secondary,
    },
    background: {
        backgroundColor: '#e6e6e6',
    },
    container: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    center: {
        margin: 'auto',
        textAlign: 'center'
    },
    topAlign: {
      display: 'flex',
      alignItems: 'top'
    }
});

class Signin extends React.Component {

    constructor(props) {
        super(props);
        document.getElementById('mainContainer').classList.add('signIn');
debugger;
        this.redirectRider = this.redirectRider.bind(this);
        this.redirectTriver = this.redirectTriver.bind(this);
    }

    componentDidMount(){
      let signInUser = this.props.getLoggedUserInfo();
      if(signInUser.userid && signInUser.token){
        if(signInUser.userType == 'Rider'){
          this.redirectRider();
        }
        else{
          this.redirectTriver();
        }
      }
    }

    redirectRider(){
      this.props.history.push("Rider");
    }

    redirectTriver(){
      this.props.history.push("Driver");
    }

    render() {
        const { classes } = this.props;
        return (
            <section className={classes.background + " mbr-section mbr-section__container article header3-c"} id="SignupLinkMain">
                <div className="container">
                    <div className={"row"}>
                        <div className={"col-xs-12"}>
                            <div className={[classes.root, classes.center]}>
                                <h3>Sign in</h3>
                                <br/>
                                <Grid container spacing={24}>
                                    <Grid item xs={12} sm={8} md={6} lg={6} className={[classes.center, classes.background]}>
                                    <h4> Driver</h4>
                                    <SigninForm usertype="Triver" updateUserInfo={this.props.updateUserInfo} redirect={this.redirectTriver}/>
                                     
                                    </Grid>
                                    <Grid item xs={12} sm={8} md={6} lg={6} className={[classes.center, classes.background]}>
                                        <h4>Rider</h4>
                                        <SigninForm usertype="Rider" updateUserInfo={this.props.updateUserInfo} redirect={this.redirectRider}/>

                                    </Grid>
                                </Grid>
                            </div>
                            <small className="mbr-section-subtitle"></small>
                        </div>
                    </div>
                </div>
            </section>
        );
    }
}

Signin.propTypes = {
    classes: PropTypes.object.isRequired,
};

// export default Signin;
export default withStyles(styles)(Signin);
