import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Paper from '@material-ui/core/Paper';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Directors from './Directors';
import Advisors from './Advisors';
import Developers from './Developers';
import Partners from './Partners'

const styles = theme => ({
    root: {
        flexGrow: 1,
        paddingTop: 20
    },
    paper: {
        padding: theme.spacing.unit * 2,
        textAlign: 'center',
        color: theme.palette.text.secondary,
    },
    background: {
        backgroundColor: '#e6e6e6',
        paddingTop: 20,
        paddingBottom: 120,
        // display: 'none'
    },
    container: {
        display: 'flex',
        flexWrap: 'wrap',
        padding: 20
    },
    button: {
        height: 10,
        marginTop: 10
    },
    tabWrpper: {
        backgroundColor: 'black'
    },
    label: {
        color: 'white',
        lineHeight: 6
    }
});

class triverTeam extends React.Component {
    constructor(props) {
        super(props);
    }

    state = {
        value: 1,
    };

    handleChange = (event, value) => {
        this.setState({ value });
        if(value ===0){
            this.props.history.push('/');
             document.getElementById('mainContainer').classList.remove('signIn');
             document.body.classList.remove('innerPage');
        }
    };

    render() {
        const { classes } = this.props;
        const { value } = this.state;
        document.body.classList.add('innerPage');
        return (
            <Paper square>
                <Tabs
                    value={this.state.value}
                    indicatorColor="primary"
                    textColor="secondary"
                    onChange={this.handleChange}
                    centered
                    className={classes.tabWrpper}
                >
                    <Tab className={classes.label} label="Main" />
                    <Tab className={classes.label} label="Board of Directors" />
                    <Tab className={classes.label} label="Board of Advisors" />
                    <Tab className={classes.label} label="Development Team"  />
                    <Tab className={classes.label} label="Patners" />
                </Tabs>
                
                {value === 1 && <Directors classes={classes}>Item One</Directors>}
                {value === 2 && <Advisors classes={classes}>Item Two</Advisors>}
                {value === 3 && <Developers classes={classes}>Item Three</Developers>}
                {value === 4 && <Partners classes={classes}>Item Three</Partners>}
            </Paper>
        );
    }
}
// Rider.propTypes = {
//     classes: PropTypes.object.isRequired,
// };

// export default Rider;
export default withStyles(styles)(triverTeam);